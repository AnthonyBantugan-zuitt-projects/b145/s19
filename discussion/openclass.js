let students = [];
let sectionedStudents = [];

function addStudent(name) {
      //call out the array and use the push() to add a new element inside the container.
      students.push(name);
      // display a return in the console.
      console.log(name + ' has been added to the list of students');
}
addStudent('Marie');
addStudent('John');
addStudent('Zyrus');
console.log(students);

function countStudents() {
	// display the number of elements inside the array with the prescribed message.
	console.log('This class has a total of ' + students.length + ' enrolled students');
}

// invocation
countStudents();

function printStudents() {
	// sort the elements inside the array in alphanumeric order.
	students = students.sort();
	// console.log(students);
	// we want to display each element in the console individually.
	students.forEach(function(student) {
		console.log(student);
	})
}
printStudents();

function findStudents(keyword) {
	// we have to describe each element inside the array individually.
	let matches = students.filter(function(student) {
		// we are assessing/evaluationg if each element includes the keyword.
		// we converted both the keyword and each element into lowercase characters.
		return student.toLowerCase().includes(keyword.toLowerCase());
	});
	console.log(matches) //this is a checker
}
findStudents('z');